import base64
import json
import logging
import sys
from time import time, sleep
from typing import List, Any, Tuple, Dict, Generator, Callable, Optional

import numpy as np
import tflite_runtime.interpreter as tflite
from datetime import datetime, timedelta
from io import BytesIO
from pathlib import Path

from PIL import Image, ImageDraw, ImageOps
from pyzbar.pyzbar import decode

from epine.config import Config
from epine.display import Display
from epine.inout import Camera, LED
from epine.labels import get_labels
from epine.network import Client, Wifi


class Analyzer:
    def __init__(self, model_path: Path, client: Client, config: Config):
        self.interpreter = tflite.Interpreter(model_path=str(model_path.absolute()))
        self.interpreter.allocate_tensors()

        self.client = client
        self.config = config
        self.previous_tags_ts: Dict[str, float] = {}
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)

    @staticmethod
    def _add_boxes(img: Image, boxes: List) -> bytes:
        draw = ImageDraw.Draw(img)
        for (y1, x1, y2, x2) in boxes:
            x1 = x1 * img.width
            x2 = x2 * img.width
            y1 = y1 * img.height
            y2 = y2 * img.height
            draw.rectangle(((x1, y1), (x2, y2)), outline=(153, 0, 0), width=5)

        buffer = BytesIO()
        img.save(buffer, format='JPEG')
        return buffer.getvalue()

    def analyze(self, img: Image) -> Tuple[bytes, List[str]]:
        if not self.config.get_notification():
            return b"", []

        resized = ImageOps.fit(img, (320, 320), Image.ANTIALIAS)
        matrix = np.array(resized).reshape((1, resized.height, resized.width, 3))

        tags, scores, boxes = self._detect(matrix)

        tags = self._convert_tags(tags, scores)
        boxes = [b for b, s in zip(boxes, scores) if s > 0.5]
        res = Analyzer._add_boxes(img, boxes)

        if self._is_time_elapsed(tags) and self._objects_enable(tags):
            self.client.send_notification(res, tags)

        self.logger.debug(f"tags detected={tags}")
        return res, tags

    def _detect(self, matrix: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        # Get input and output tensors.
        input_details = self.interpreter.get_input_details()
        output_details = self.interpreter.get_output_details()

        self.interpreter.set_tensor(input_details[0]['index'], matrix)
        self.interpreter.invoke()

        boxes = self.interpreter.get_tensor(output_details[0]['index'])[0]
        tags = self.interpreter.get_tensor(output_details[1]['index'])[0]
        scores = self.interpreter.get_tensor(output_details[2]['index'])[0]
        return tags, scores, boxes

    def _is_time_elapsed(self, tags: List[str]) -> bool:
        now = time()
        elapsed = False

        for tag in tags:
            if now - self.previous_tags_ts.get(tag, 0) > self.config.get_detection_delay() * 60:
                elapsed = True

        if elapsed:
            for tag in tags:
                self.previous_tags_ts[tag] = now

        return elapsed

    def _objects_enable(self, tags: List[str]) -> bool:
        return any(t not in self.config.get_disable_objects() for t in tags)

    @staticmethod
    def _convert_tags(tags: np.ndarray, scores: np.ndarray) -> List[str]:
        upper_tags = [get_labels(i) for i, s in zip(tags, scores) if s > 0.5]
        return list(set(upper_tags))


class Brightness:
    TARGET = 0.20
    DARK = 0.10
    LIGHT = 0.30

    def __init__(self, config: Config, led: LED, size: int = 10):
        self.config = config
        self.states = [Brightness.LIGHT] * size
        self.led = led

    def update(self, img: Image):
        br = Brightness._calculate_brightness(img)
        print("BRG=", br)

        self.states.append(br)
        del self.states[0]

        mean = sum(self.states) / len(self.states)
        print("MEAN=", mean)
        if mean > Brightness.LIGHT:
            self.led.off()
        if mean < Brightness.DARK:
            self.led.on()

        return mean


    @staticmethod
    def _calculate_brightness(image: Image) -> float:
        greyscale_image = image.convert('L')
        histogram = greyscale_image.histogram()
        pixels = sum(histogram)
        brightness = scale = len(histogram)

        for index in range(0, scale):
            ratio = histogram[index] / pixels
            brightness += ratio * (-scale + index)

        return 1 if brightness == 255 else brightness / scale


class Watcher:
    def __init__(self, config: Config, client: Client):
        self.config = config
        self.last_shot = None
        self.client = client
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)

        self.reset()

    def reset(self):
        self.last_shot = datetime(year=1970, month=1, day=1)

    def shot(self, img: Image) -> dict:
        if datetime.now() - self.last_shot > timedelta(minutes=self.config.get_timelapse_delay()):
            self.logger.debug("watcher keep current image")
            self.last_shot = datetime.now()
            buffer = BytesIO()
            img.save(buffer, format="jpeg")
            return self.client.send_shot(img=buffer.getvalue())


class QRCode:
    def __init__(self, config: Config, wifi: Wifi, client: Client, display: Display):
        self.config = config
        self.client = client
        self.wifi = wifi
        self.display = display
        self.latest_message = ""
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)

    def scan(self, img: Image) -> bool:
        data = QRCode._extract_code(img)
        if len(data) > 0 and self.latest_message != data:
            self.display.info("QRCode scanned")
            self._match(json.loads(data))

            self.client.send_ack(data)
            self.latest_message = data
            return True

        else:
            return False

    def _match(self, mes: dict):
        if 'wifi_ssid' in mes and 'wifi_password' in mes and 'country_code' in mes:
            self.logger.debug(f"Wifi config received for ssid={mes['wifi_ssid']}")
            self.config.set_wifi(ssid=mes['wifi_ssid'], pwd=mes['wifi_password'], country=mes['country_code'])
            self.wifi.connect()
        if 'key' in mes:
            self.config.set_key(key=mes['key'])
            self.logger.debug("new key setup")
            self.display.valid("new key added", id="key")
        if 'url' in mes:
            url = base64.b64decode(mes['url'].encode()).decode('ascii')
            self.config.set_server_url(url=url)
            self.logger.debug(f"url config received for url={url}")
            self.display.valid("new server added", id="server")

    @staticmethod
    def _extract_code(img: Image) -> str:
        codes = decode(img)
        if len(codes) > 0:
            return codes[0].data.decode('utf-8')
        else:
            return ''


class LiveStream:
    PULSE_SHUTDOWN_SEC = 30

    def __init__(self, config: Config, client: Client):
        self.config = config
        self.client = client
        self.last_pulse = 0

    def recording(self, img: bytes) -> bool:
        if time() - self.last_pulse > LiveStream.PULSE_SHUTDOWN_SEC:
            return False

        self.client.send_live(img)
        return True

    def update_pulse(self, *args, **kwargs):
        self.last_pulse = int(time())


class SettingsUpdate:
    def __init__(self, config: Config, display: Display):
        self.config = config
        self.display = display
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)

    def update_event(self, type: str, body: Dict[str, Any]) -> None:
        if type != "SETTINGS":
            return

        if "notification" in body:
            self.config.set_notification(body["notification"])
            self.logger.debug("Notification updated")
        if "delayTimelapseMn" in body:
            self.config.set_timelapse_delay(body["delayTimelapseMn"])
            self.logger.debug("Timelapse delay updated")
        if "delayDetectionMn" in body:
            self.config.set_detection_delay(body["delayDetectionMn"])
            self.logger.debug("Detection delay updated")
        if "disableObjects" in body:
            self.config.set_disable_objects(body["disableObjects"])
            self.logger.debug("Disable objects updated")


        self.display.valid("Settings updated")


class Workflow:
    def __init__(self,
                 qrcode: QRCode,
                 watcher: Watcher,
                 analyzer: Analyzer,
                 brightness: Brightness,
                 live: LiveStream,
                 camera: Camera,
                 client: Client,
                 display: Display):
        self.qrcode = qrcode
        self.watcher = watcher
        self.analyzer = analyzer
        self.brightness = brightness
        self.live = live
        self.camera = camera
        self.client = client
        self.display = display
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)

    def run(self) -> Generator[dict, Any, None]:
        for stream in self.camera.capture():
            yield self._sub_run(stream)

    def _sub_run(self, stream: bytes) -> Dict[str, Any]:
        try:
            img = Image.open(BytesIO(stream))
            self.brightness.update(img.copy())

            if self.live.recording(stream):
                return {}

            sleep(0.1)

            if self.qrcode.scan(img.copy()):
                self.watcher.reset()
                return {}

            self.analyzer.analyze(img.copy())
            return self.watcher.shot(img.copy())
        except Exception as e:
            self.logger.error(e)
            return {}

    @staticmethod
    def reduce_size(img: Image) -> Image:
        return ImageOps.fit(img, (1000, 1000), Image.ANTIALIAS)

    def stop(self):
        self.client.stop()
        self.display.stop()

