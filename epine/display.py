import base64
import logging
import textwrap
import io
import threading
from dataclasses import dataclass
from pathlib import Path
from time import sleep
from typing import Any, Tuple, List, Dict, Set

from PIL import Image, ImageDraw, ImageFont

from epine import __version__, crypto
from epine.config import Config


@dataclass(init=True, repr=True, frozen=True)
class Message:
    icon: str
    text: str


class Screen:
    WIDTH = 128
    HEIGHT = 32

    def __init__(self, out: Any, font_path: Path, delay: int = 3):
        self.out = out
        self.font = str(font_path.absolute())
        self.delay = delay
        self.icons = {
            "error": base64.b64decode("iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAZUlEQVR4AeXRMRGAQBAEwQ8IEECAy5fyQpCBCCQgo1HAchQhG191cNN+NMyfb7DgQA83A3vEMGED9BsERosLWB0JWB3JmNdI+An0r8hIAcpIrllGQs00rDgxcgAH5kesUHNp/9kFUU/w1U8vqWgAAAAASUVORK5CYII="),
            "valid": base64.b64decode("iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAATElEQVR4AWOgHRgF////LwdiCUoN6f8PAeXUMKR/iBgCCjggXg+iyTMEoVgDiJ8D8XWQYfgMIdkwbIaQY9h7dEMoMayf4pQLD6dRAACupZnfyahNIgAAAABJRU5ErkJggg=="),
            "pending": base64.b64decode("iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAyElEQVR4Aa3TAWYDQRSA4aCARY/RQxQFoIAAeogigKIoIOgBggKKIiiKRZEDLAIIgiAHCILgi+QhazA7MvmBXc/HvDGjoXA3qisQ/NwEgVro26UOMzxfA41xEM2xEX2hKV9sD+sddRK4dgh6xWeKJd8T0TgHdfgvuIQNfnNDe7wX7G+OdWbg3LQAmg1BK7QF0BJ/uYEP0VPy/zHZ0QIvOeg+Fmnbx7BLsGbwDeIBnajFVBTYFe/sDYsAothLRaIT2tRCgdRWghwBBGszrZ84wN0AAAAASUVORK5CYII="),
            "info": base64.b64decode("iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAAXNSR0IArs4c6QAAAFBlWElmTU0AKgAAAAgAAgESAAMAAAABAAEAAIdpAAQAAAABAAAAJgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAAEqADAAQAAAABAAAAEgAAAACIdJxLAAABWWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNi4wLjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgoZXuEHAAAAWElEQVQ4EWNgoBf4//9/MBDHAzEL2XYCNTsDMQzE4zOICZ8kUO4jkvwXJDbpTKBzTIDYgXSdtNABdEk9EMNAPT47CIURPr0ocqMGoQQHVs4wDiOs/h0eggC2Dy+XsuhQUQAAAABJRU5ErkJggg=="),
            "wifi": base64.b64decode("iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAyklEQVR4AeXRAQTCQBjF8SEMQ4AQQoAQAgSEEAaAAAABhgABYDgEgDAMEAIMAAAhQAghDBCGYf3RRx43E8AePzPnvrvngp6laZoIMfZwSJFgjbDLgDkuqOBLhQxT3w1OsuGBDA5H5HjBUiPFwIYMcYUlx7zl1ksUsBSIbNGhxEo2LRBjg5msbfGG05NG32+IA0ponthZHb6TQKM1f3LDHUTr+GM1KySI5JAUtdZpGzaWFw3tX57dG615hj113lanQ02L1PlrGIL+5gOyJXzivZZHpQAAAABJRU5ErkJggg=="),
            "key": base64.b64decode("iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAoUlEQVR4AWMgDYyC////iwCxNxDHA7EBuYYUA/H3/6hgPxBLkGJI9n/c4DIQsxBjCAsQf0bSeB2ItwPxbySx+UBcjwcrgAwyQNLwHGY7kO7+TzxwAGkwQRK4j+TSalIN4kDzxm4gno7m3cVI3piPbDHcayAA5uAG29HC1AE5VrEF+GSYLFrAixBlEJoiHVBSgLrQH1u0g7yB5M144lPqKAAAdP3D6FvMO1YAAAAASUVORK5CYII="),
            "server": base64.b64decode("iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAzElEQVR4AeXSgQbCUBSA4WEYQugBeoQBQm8QoIcYAgQIQwjDEAYIYRjCEAIMEGAYhiGEEMIw3P6Dy0RzW0D9fKRrZ5sd6w9TSs1xRIUcMaafDBgghVQgxgFXSJHpoD0aeLBb/zsIIPnvLraxRAZp03GjBDWGrwcOTpBu2GLYMciFFGLcPlhBWsAxeHUbD+gCfVAi7fFVXewgeZYiWvdcERsXnPUT5V/sW4JKfoSQZj2GjFAj0QtYoEEM31CEOxq47W0OUcK0Ghkm1m/3BEhUfEvm9cDbAAAAAElFTkSuQmCC")
        }
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)

    def reset(self) -> Image:
        self.out.fill(0)
        self.out.show()
        return Image.new("1", (Screen.WIDTH, Screen.HEIGHT))

    def _show(self, image: Image):
        image = image.rotate(180)
        self.out.image(image)
        self.out.show()

    def show_splash(self):
        image = self.reset()
        draw = ImageDraw.Draw(image)

        # Show Epine
        font = ImageFont.truetype(self.font, 20)
        text = "épine"
        (font_width, font_height) = font.getsize(text)

        draw.text(
            (Screen.WIDTH // 2 - font_width // 2, Screen.HEIGHT // 2 - font_height // 2),
            text,
            font=font,
            fill=255,
        )

        self._show(image)
        sleep(self.delay)

    def show_message(self, mes: Message):
        self.logger.debug(mes)

        image = self.reset()
        draw = ImageDraw.Draw(image)

        s = Screen.HEIGHT * 2 // 3
        icon = Image.open(io.BytesIO(self.icons[mes.icon]))
        image.paste(icon, (s // 2, s // 2 - 3))

        font = ImageFont.truetype(self.font, 10)
        lines = textwrap.wrap(mes.text, width=14)
        for i, text in enumerate(lines):
            w, h = font.getsize(text)
            h_line = Screen.HEIGHT / len(lines)

            # Draw text
            draw.text(
                ((Screen.WIDTH + Screen.HEIGHT - w) // 2, h_line * i + (h_line - h) // 2),
                text=text, font=font, fill=255,
            )

        self._show(image)
        sleep(self.delay)


class Display:
    def __init__(self, screen: Screen, config: Config):
        self._config = config
        self._screen = screen
        self._passing: List[Message] = list()
        self._stuck: Dict[str, Message] = dict()
        self._resume = True

        self.lock = threading.Lock()
        thread = threading.Thread(target=self._run)
        thread.start()

    def _manage_stuck(self, id: str, mes: Message = None):
        def f(id: str, mes: Message):
            with self.lock:
                if mes:
                    self._stuck[id] = mes
                else:
                    del self._stuck[id]
        threading.Thread(target=f, args=(id, mes)).start()

    def pending(self, mes: str, id: str):
        self._manage_stuck(id, Message(icon="pending", text=mes))

    def valid(self, mes: str, id: str = ''):
        if id in self._stuck:
            self._manage_stuck(id)

        self._passing.append(Message(icon='valid', text=mes))

    def error(self, mes: str, id: str):
        self._manage_stuck(id, Message(icon="error", text=mes))

    def info(self, mes: str):
        self._passing.append(Message(icon="info", text=mes))

    def config(self):
        self._passing.append(Message(icon="info", text=f"v{__version__}"))

        wifi, _, _ = self._config.get_wifi()
        if wifi:
            self._passing.append(Message(icon="wifi", text=wifi))
        else:
            self.error("no wifi setup", id="wifi")

        server = self._config.get_server_url()
        if server:
            self._passing.append(Message(icon="server", text=server))
        else:
            self.error("no server setup", id="wifi")

        key = self._config.get_key()
        if key:
            id = crypto.map_key(key).upper()[0:8]
            self._passing.append(Message(icon="key", text=id))
        else:
            self.error("no key setup", id="key")

    def stop(self):
        self._resume = False

    def _run(self):
        self._screen.show_splash()
        while self._resume:
            with self.lock:
                for error in self._stuck.values():
                    self._screen.show_message(error)
            for news in self._passing:
                self._screen.show_message(news)
                self._passing.remove(news)
                self._screen.reset()
            sleep(.1)


