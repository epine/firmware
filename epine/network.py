import base64
import json
import logging
import subprocess
import threading
from json.decoder import JSONDecodeError
from time import sleep
from typing import List, Dict, Callable, Any

import requests

from pythonping import ping

from epine import crypto
from epine.config import Config
from epine.crypto import decrypt
from epine.display import Display

requests.adapters.DEFAULT_RETRIES = 5


class Client:
    def __init__(self, config: Config):
        self.config = config
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)
        self.listeners = []
        self.resume = True

        self.thread = threading.Thread(target=self._listen)

    def start(self):
        self.thread.start()

    def stop(self):
        self.resume = False

    def _listen(self):
        while self.resume:
            key = self.config.get_key()
            base = self.config.get_server_url()
            camera_id = crypto.map_key(key)

            try:
                res = requests.get(f"{base}/api/v1/camera/{camera_id}/event?session=0", timeout=60)

                if res.status_code == 200 and key != "":
                    data = res.json()
                    self.logger.debug("new event received")
                    for listener in self.listeners:
                        body = json.loads(decrypt(stream=base64.b64decode(data["body"]), key=key))
                        listener(data["type"], body)
            except:
                pass

    def decode(self, r) -> dict:
        try:
            res = json.loads(r.content)
            return res
        except JSONDecodeError:
            self.logger.error(f"enable to decode response {r.content} ")
            return {}

    def post(self, url: str, headers: Dict[str, str] = None, files: Dict[str, bytes] = None):
        try:
            r = requests.post(url, headers=headers, files=files)
            logging.debug(f"{r.status_code} POST {url}")
            return self.decode(r)
        except IOError as e:
            self.logger.error(f"FAIL POST {url}")
            self.logger.debug(e)
            return {}

    def send_shot(self, img: bytes, live: bool = False) -> dict:
        key = self.config.get_key()
        base = self.config.get_server_url()
        if not key or not base:
            return {}

        camera_id = crypto.map_key(key)
        url = f'{base}/api/v1/camera/{camera_id}/{"live" if live else "shot"}'
        headers = {'Content-Length': str(len(img))}
        return self.post(url, headers=headers, files={'image': crypto.encrypt(key, img)})

    def send_ack(self, message: str):
        key = self.config.get_key()
        base = self.config.get_server_url()
        if not key or not base:
            return {}

        camera_id = crypto.map_key(key)
        message_id = crypto.sha256(message)
        url = f"{base}/api/v1/camera/{camera_id}/ack/{message_id}"
        return self.post(url)

    def send_notification(self, img: bytes, tags: List[str]):
        key = self.config.get_key()
        base = self.config.get_server_url()
        if not key or not base:
            return {}

        camera_id = crypto.map_key(key)
        tags = crypto.encrypt(key, ",".join(tags).encode())
        tags = base64.urlsafe_b64encode(tags).decode()
        img = crypto.encrypt(key, img)

        url = f'{base}/api/v1/camera/{camera_id}/notification/{tags}'
        headers = {}

        return self.post(url, headers=headers, files={'image': img})

    def add_event_listener(self, listener: Callable[[str, Dict[str, Any]], Any]) -> None:
        self.listeners.append(listener)

    def send_live(self, img: bytes):
        return self.send_shot(img, live=True)


class Wifi:
    WPA_SUPPLICANT = """country=%s
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
ssid="%s"
psk="%s"
}"""

    def __init__(self, config: Config, display: Display,
                 f='/etc/wpa_supplicant/wpa_supplicant.conf',
                 cmd='sudo wpa_cli -i wlan0 reconfigure'):
        self.config = config
        self.display = display
        self.f = f
        self.cmd = cmd
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)

    def connect(self):
        ssid, pwd, country = self.config.get_wifi()
        if ssid == '' or pwd == '' or country == '':
            return

        self.display.pending("Connecting to wifi...", id="wifi")

        content = Wifi.WPA_SUPPLICANT % (country, ssid, pwd)
        with open(self.f, 'w') as f:
            f.write(content)

        subprocess.call(self.cmd.split(' '))
        domain = self.config.get_server_url().split('://')[-1]
        for _ in range(20):
            try:
                if ping(domain).success():
                    self.logger.debug(f"Wifi connected to {ssid}")
                    self.display.valid("Conected to wifi", id="wifi")
                    return
            except Exception:
                sleep(1)
        self.logger.error(f"Can't connect to wifi {ssid}")
        self.display.error(f"Can't connect to {ssid}", id="wifi")



