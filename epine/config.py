import json
from abc import ABC, abstractmethod
from pathlib import Path
from threading import Lock
from typing import Tuple, Dict, Any, List


class Config(ABC):
    DEFAULT = {
        "server_url": "https://api.epine.io",
        "timelapse_delay": 20,
        "detection_delay": 5 * 60,
        "notification": True,
        "disable_objects": []
    }

    def __init__(self, config: Dict[str, Any] = None):
        config = config or {}
        self.config = dict(Config.DEFAULT, **config)  # Union
        self._save()

    @abstractmethod
    def _save(self):
        pass

    def set_key(self, key: str):
        self.config['key'] = key
        self._save()

    def get_key(self) -> str:
        return self.config.get('key', '')

    def set_wifi(self, ssid: str, pwd: str, country: str):
        self.config['wifi_ssid'] = ssid
        self.config['wifi_pwd'] = pwd
        self.config['wifi_country'] = country
        self._save()

    def get_wifi(self) -> Tuple[str, str, str]:
        return self.config.get('wifi_ssid', ''), self.config.get('wifi_pwd', ''), self.config.get('wifi_country')

    def set_timelapse_delay(self, delay: int):
        self.config['timelapse_delay'] = delay
        self._save()

    def get_timelapse_delay(self) -> int:
        return self.config.get('timelapse_delay', 20)

    def set_detection_delay(self, delay: int):
        self.config['detection_delay'] = delay
        self._save()

    def get_detection_delay(self) -> int:
        return self.config.get('detection_delay', 5 * 60)

    def set_notification(self, value: bool):
        self.config["notification"] = value
        self._save()

    def get_notification(self) -> bool:
        return self.config.get("notification", True)

    def set_disable_objects(self, objects: List[str]):
        self.config["disable_objects"] = objects
        self._save()

    def get_disable_objects(self) -> List[str]:
        return self.config.get("disable_objects", [])

    def set_server_url(self, url: str):
        self.config['server_url'] = url
        self._save()

    def get_server_url(self) -> str:
        return self.config.get('server_url', '')


class PersistentConfig(Config):
    def __init__(self, file):
        self.path = Path(file)
        self.lock = Lock()
        config = json.loads(self.path.read_text())  if self.path.exists() else None
        super(PersistentConfig, self).__init__(config)

    def _save(self):
        self.lock.acquire()
        self.path.write_text(json.dumps(self.config))
        self.lock.release()
