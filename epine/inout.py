import io
import logging
import time
from typing import Generator, Any, Optional


class Camera:
    SIZE = (640, 640)  # (2592, 1944)

    def __init__(self, picamera):
        self.picamera = picamera
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)

    def capture(self) -> Generator[bytes, Any, None]:
        with self.picamera:
            self.picamera.resolution = Camera.SIZE
            self.picamera.start_preview()
            self.picamera.rotation = 270
            time.sleep(2)

            stream = io.BytesIO()
            for _ in self.picamera.capture_continuous(stream, format='jpeg', use_video_port=True):
                stream.seek(0)
                data = stream.read()
                stream.seek(0)
                stream.truncate()
                self.logger.debug(f"Shot taken size={len(data)}")
                yield data


class LED:
    PIN = 17

    def __init__(self, gpio):
        self.gpio = gpio
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)

        self.gpio.setmode(self.gpio.BCM)
        self.gpio.setup(LED.PIN, self.gpio.OUT)

    def on(self):
        self.gpio.output(LED.PIN, self.gpio.HIGH)

    def off(self):
        self.gpio.output(LED.PIN, self.gpio.LOW)
