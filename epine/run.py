#! /usr/bin/python3.7

import platform
import sys
from pathlib import Path
from time import sleep
from typing import Tuple, Any
from unittest.mock import Mock

from epine.display import Display, Screen, Message

if platform.system() == 'Linux' and platform.architecture() == ('32bit', ''):
    import picamera
    import board
    import busio
    from adafruit_ssd1306 import SSD1306_I2C as OLED
    import RPi.GPIO as gpio


from epine import __version__
from epine.config import PersistentConfig, Config
from epine.inout import Camera, LED
from epine.network import Client, Wifi
from epine.services import Watcher, QRCode, Analyzer, Workflow, Brightness, SettingsUpdate, LiveStream


def build(config: Config = None,
          screen: Screen = None,
          wifi: Wifi = None,
          pi_camera: Any = None,
          led: Any = None,
          res: Path = None) -> Workflow:

    res = res or Path(sys._MEIPASS) / "res"
    config = config or PersistentConfig(file='config.json')
    screen = screen or Screen(
                        out=OLED(Screen.WIDTH, Screen.HEIGHT, busio.I2C(board.SCL, board.SDA)),
                        font_path=res / "Roboto.ttf")

    # Output interface
    display = Display(screen=screen, config=config)
    led = led or LED(gpio=gpio)
    wifi = wifi or Wifi(config, display=display)
    client = Client(config=config)

    # Input interfaces
    pi_camera = pi_camera or picamera.PiCamera()
    camera = Camera(picamera=pi_camera)

    # Services
    analyzer = Analyzer(model_path=res / "detect.tflite", client=client, config=config)
    watcher = Watcher(config=config, client=client)
    qrcode = QRCode(config=config, wifi=wifi, client=client, display=display)
    brightness = Brightness(config=config, led=led)
    live = LiveStream(config=config, client=client)
    su = SettingsUpdate(config=config, display=display)

    client.add_event_listener(su.update_event)
    client.add_event_listener(live.update_pulse)
    client.start()

    return Workflow(
        analyzer=analyzer,
        watcher=watcher,
        qrcode=qrcode,
        brightness=brightness,
        live=live,
        camera=camera,
        client=client,
        display=display)


def run():
    workflow = build()
    workflow.display.config()

    for _ in workflow.run():
        pass


if __name__ == '__main__':
    if '-v' in sys.argv:
        print(__version__)

    elif '-u' in sys.argv:
        _, _, version = sys.argv
        res = Path(sys._MEIPASS) / "res"
        screen = Screen(
            out=OLED(Screen.WIDTH, Screen.HEIGHT, busio.I2C(board.SCL, board.SDA)),
            font_path=res / "Roboto.ttf")
        screen.show_message(Message(icon="pending", text=f"updating to {version}"))

    else:
        run()
