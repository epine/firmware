import base64
import hashlib
from typing import Union

from Crypto.Cipher import ChaCha20_Poly1305


def sha256(mes: Union[bytes, str]) -> str:
    data = mes if isinstance(mes, bytes) else mes.encode()
    return hashlib.sha256(data).hexdigest()


def map_key(key: str) -> str:
    key = base64.b64decode(key)
    return sha256(key)


def encrypt(key: str, stream: bytes) -> bytes:
    key = base64.b64decode(key)
    cipher = ChaCha20_Poly1305.new(key=key)
    data, tags = cipher.encrypt_and_digest(stream)
    return cipher.nonce + data + tags


def decrypt(key: str, stream: bytes) -> bytes:
    key = base64.b64decode(key)
    iv = stream[:12]
    tags = stream[-16:]
    cipher = ChaCha20_Poly1305.new(key=key, nonce=iv)
    return cipher.decrypt_and_verify(stream[12:-16], received_mac_tag=tags)