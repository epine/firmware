#! /bin/bash
cd /home/pi/epine
base="https://dist.epine.io"
exe="firmware"

if [ -f $exe ];
then
  curr=$(sha1sum $exe | cut -d' ' -f1)
else
  curr="0"
fi

server=$(curl $base/sha1sum | cut -d' ' -f1)

if [ $curr = $server ];
then
    exit 0
fi

version=$(curl $base/version)
echo "new version $version available"
./$exe -u $version

wget $base/$exe -O $exe.new
chmod +x $exe.new

new=$(sha1sum $exe.new | cut -d' ' -f1)
if [ $new = $server ];
then
    echo "Download success"
else
    echo "Error during downloading : new firmaware ($new) hashs not equals"
    exit 0
fi

echo "replace new version"
systemctl stop epine
mv $exe.new $exe
systemctl start epine