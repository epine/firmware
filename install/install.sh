sudo apt update
sudo apt-get install -y libzbar0

mkdir epine
cd epine

wget https://gitlab.com/epine/firmware/-/raw/master/install/boot.sh -O boot
chmod +x boot
sudo echo "*/10 * * * * /home/pi/epine/boot" > /tmp/epine
sudo crontab /tmp/epine

wget https://gitlab.com/epine/firmware/-/raw/master/install/epine.service
sudo mv epine.service /etc/systemd/system/
sudo chmod 640 /etc/systemd/system/epine.service

sudo systemctl daemon-reload
sudo systemctl enable epine

sudo ./boot

sudo reboot


