from io import StringIO, BytesIO
from pathlib import Path
from time import sleep, time
from unittest.mock import Mock, Mock, call

import json
import os
import unittest

from PIL import Image

from epine.config import Config
from epine.network import Client, Wifi
from epine.services import QRCode, Watcher, Analyzer, Brightness, LiveStream
from tests.utils import MockConfig, get_resource


class TestAnalyzer(unittest.TestCase):
    def setUp(self) -> None:
        self.project = Path(os.path.dirname(__file__)).parent
        self.config = MockConfig(config={"detection_delay": 2})

    def test_analyze_dog(self):
        model = self.project / "epine/res/detect.tflite"
        data = get_resource("dog.jpg")

        client = Mock()

        t0 = time()
        analyzer = Analyzer(model_path=model, client=client, config=self.config)
        img, tags = analyzer.analyze(data)
        print('dog t=', (time() - t0) / 1000, 's', tags)
        client.send_notification.assert_called_once_with(img, tags)

    def test_analyze_person(self):
        model = self.project / "epine/res/detect.tflite"
        data = get_resource("humain.jpg")

        client = Mock()

        t0 = time()
        analyzer = Analyzer(model_path=model, client=client, config=self.config)
        img, tags = analyzer.analyze(data)
        print('humain t=', (time() - t0) / 1000, 's', tags)
        client.send_notification.assert_called_once_with(img, tags)

    def test_is_time_elapsed(self):
        model = self.project / "epine/res/detect.tflite"
        data = get_resource("humain.jpg")

        client = Mock()

        analyzer = Analyzer(model_path=model, client=client, config=self.config)
        img, tags = analyzer.analyze(data.copy())
        analyzer.analyze(data.copy())
        client.send_notification.count = 1
        sleep(1)
        analyzer.analyze(data.copy())
        client.send_notification.count = 2

    def test_convert_tags(self):
        tags = [15, 1, 56, 3, 4]
        scores = [.3, .7, .8, .1, .51]

        res = Analyzer._convert_tags(tags, scores)

        self.assertEqual(set(res), {'airplane', 'carrot', 'bicycle'})


class TestBrightness(unittest.TestCase):
    def test_update(self):
        dark = get_resource("dark.jpeg")
        light = get_resource("humain.jpg")
        led = Mock()

        brightness = Brightness(config=MockConfig(config={}), led=led)

        self.assertAlmostEqual(brightness.update(dark), 0.28, delta=0.01)
        self.assertAlmostEqual(brightness.update(dark), 0.27, delta=0.01)
        self.assertAlmostEqual(brightness.update(light), 0.29, delta=0.01)
        self.assertAlmostEqual(brightness.update(light), 0.32, delta=0.01)
        led.off.assert_called_once_with()


    def test_compute_brightness(self):
        def compute(file: str) -> float:
            return Brightness._calculate_brightness(get_resource(file))

        self.assertAlmostEqual(0.15, compute("dark.jpeg"), delta=0.01)
        self.assertAlmostEqual(0.33, compute("sun.jpeg"), delta=0.01)
        self.assertAlmostEqual(0.55, compute("humain.jpg"), delta=0.01)


class TestWatcher(unittest.TestCase):
    def test_wait(self):
        # Mock
        config = MockConfig(config=dict(timelapse_delay=1 / 60))
        client = Client(config)
        client.send_shot = Mock()

        # Input
        img = get_resource('noqrcode.jpeg')

        # Test
        watcher = Watcher(config=config, client=client)
        watcher.shot(img)
        d = watcher.last_shot

        sleep(.5)
        watcher.shot(img)
        self.assertEqual(d, watcher.last_shot)

        sleep(.5)
        watcher.shot(img)
        self.assertNotEqual(d, watcher.last_shot)

        self.assertEqual(client.send_shot.call_count, 2)


class TestLiveStream(unittest.TestCase):
    def test_recording(self):
        # Mock
        config = MockConfig(config={})
        client = Mock()
        client.send_live.return_value = True

        live = LiveStream(config=config, client=client)
        img = get_resource('dark.jpeg')

        self.assertFalse(live.recording(img))
        client.send_live.assert_not_called()

        live.update_pulse()
        self.assertTrue(live.recording(img))
        client.send_live.assert_called_once()


class TestQRCode(unittest.TestCase):
    def test_extract_qrcode(self):
        img = get_resource('qrcode.jpeg')
        d = QRCode._extract_code(img)

        # data present in qrcode.jpeg
        self.assertEqual('{"key":"ZpTLAZBy/zSupkcT0Lb18t+ouoLnve1cp4fqzeuEZzs="}', d)

    def test_extract_noqrcode(self):
        img = get_resource('noqrcode.jpeg')
        d = QRCode._extract_code(img)

        # data present in qrcode.jpeg
        self.assertEqual('', d)

    def test_match_wifi(self):
        # Mock
        config = MockConfig(config={})
        config.set_wifi = Mock()

        wifi = Mock()

        # Input
        mes = {'wifi_ssid': 'my-ssid', 'wifi_password': 'my-pwd', 'country_code': 'FR'}

        # Test
        qrcode = QRCode(config=config, wifi=wifi, client=Mock(), display=Mock())
        qrcode._match(mes=mes)

        config.set_wifi.assert_called_once_with(ssid='my-ssid', pwd='my-pwd', country='FR')
        wifi.connect.assert_called_once_with()

    def test_match_key_two_call(self):
        # Mock
        config = MockConfig(config={})
        config.set_key = Mock()

        # input
        mes = {'key': 'my-key'}

        # Test
        display = Mock()
        qrcode = QRCode(config=config, wifi=Mock(), client=Mock(), display=display)
        qrcode._match(mes=mes)

        config.set_key.assert_called_once_with(key='my-key')
        display.valid.assert_called_once_with("new key added", id="key")

    def test_match_url(self):
        # Mock
        config = MockConfig(config={})
        config.set_server_url = Mock()

        # input
        mes = {'url': 'aHR0cDovL2xvY2FsaG9zdDo2OTY1'}

        # Test
        display = Mock()
        qrcode = QRCode(config=config, wifi=Mock(), client=Mock(), display=display)
        qrcode._match(mes=mes)

        config.set_server_url.assert_called_once_with(url='http://localhost:6965')
        display.valid.assert_called_once_with("new server added", id="server")

    def test_scan(self):
        # Mock
        config = MockConfig(config=dict(delay=1))
        config.set_key = Mock()
        display = Mock()

        # input
        img = get_resource('qrcode.jpeg')

        # Test
        qrcode = QRCode(config=config, wifi=Mock(), client=Mock(), display=display)
        qrcode.scan(img)

        config.set_key.assert_called_with(key='ZpTLAZBy/zSupkcT0Lb18t+ouoLnve1cp4fqzeuEZzs=')
        display.info.assert_called_once_with("QRCode scanned")
