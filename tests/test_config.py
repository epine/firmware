import json
import os
import unittest

from epine.config import PersistentConfig


class TestConfig(unittest.TestCase):
    FILE = 'test-config.json'

    def tearDown(self) -> None:
        if os.path.exists(TestConfig.FILE):
            os.remove(TestConfig.FILE)

    def test_config_exist(self):
        # Input
        elements = ['key', 'wifi_ssid', 'wifi_pwd', 'wifi_country']
        with open(TestConfig.FILE, 'w') as f:
            json.dump({e: '%s_created' % e for e in elements}, f)

        # Test
        config = PersistentConfig(TestConfig.FILE)
        self.assertEqual('key_created', config.get_key())
        self.assertEqual(('wifi_ssid_created', 'wifi_pwd_created', 'wifi_country_created'), config.get_wifi())
        self.assertEqual('https://api.epine.io', config.get_server_url())
        self.assertEqual(20, config.get_timelapse_delay())

        config.set_key('key_updated')
        config.set_wifi('wifi_ssid_updated', 'wifi_pwd_updated', 'wifi_country_updated')
        config.set_server_url('server_url_updated')
        config.set_timelapse_delay('timelapse_delay_updated')
        config.set_detection_delay('detection_delay_updated')
        config.set_notification('notification_updated')
        config.set_disable_objects("disable_objects_updated")

        elements = elements + ['timelapse_delay', 'detection_delay', 'server_url', 'disable_objects', 'notification']
        with open(TestConfig.FILE, 'r') as f:
            self.assertEqual({e: '%s_updated' % e for e in elements}, json.load(f))

    def test_config_not_found(self):
        config = PersistentConfig(TestConfig.FILE)
        self.assertEqual('https://api.epine.io', config.get_server_url())
        self.assertEqual(20, config.get_timelapse_delay())

