import multiprocessing
import os
import re
import unittest
from http.server import HTTPServer
from pathlib import Path
from random import randint
from time import sleep
from unittest.mock import Mock

from epine.display import Message
from epine.run import build
from tests.utils import MockServerHandler, MockConfig, get_resource, MockScreen, MockPiCamera


class IntegrationTest(unittest.TestCase):
    def setUp(self) -> None:
        self.res = Path(os.path.dirname(__file__)).parent / "epine/res"

        # Start server
        self.port = randint(0, 100) + 7800
        httpd = HTTPServer(('localhost', self.port), MockServerHandler)
        self.server = multiprocessing.Process(args=(httpd,), target=lambda x: x.serve_forever())
        self.server.start()

    def tearDown(self) -> None:
        self.server.terminate()

    def test_scan(self):
        # Linking
        config = MockConfig(config={'timelapse_delay': 0, 'server_url': f'http://localhost:{self.port}'})
        wifi = Mock()
        screen = MockScreen()
        picamera = MockPiCamera(images=['qrcode-it.jpeg'])
        workflow = build(config=config, screen=screen, wifi=wifi, res=self.res, led=Mock(), pi_camera=picamera)

        r = workflow.run().__next__()

        # Verify config
        self.assertEqual('ZpTLAZBy/zSupkcT0Lb18t+ouoLnve1cp4fqzeuEZzs=', config.get_key())
        self.assertEqual(('my-ssid', 'my-pwd', ''), config.get_wifi())

        # Verify mock
        wifi.connect.assert_called_once()

        # Verify display
        sleep(.5)
        self.assertEqual(screen.messages, [
            Message(icon='info', text='QRCode scanned'),
            Message(icon='valid', text='new key added'),
        ])

        workflow.stop()

    def test_timelapse(self):
        # Linking
        config = MockConfig(config={
            'timelapse_delay': 0,
            'server_url': f'http://localhost:{self.port}',
            'key': 'ZpTLAZBy/zSupkcT0Lb18t+ouoLnve1cp4fqzeuEZzs='
        })
        picamera = MockPiCamera(images=['noqrcode.jpeg'])
        workflow = build(config=config, screen=Mock(), wifi=Mock(), res=self.res, led=Mock(), pi_camera=picamera)

        r = workflow.run().__next__()

        # verify photo and encryption
        uuid4hex = re.compile('[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}', re.I)
        self.assertTrue(bool(uuid4hex.match(r['id'])))
        self.assertTrue(r['date'] >= 1600000000)

        workflow.stop()

    def test_live(self):
        # Linking
        config = MockConfig(config={
            'timelapse_delay': 0,
            'server_url': f'http://localhost:{self.port}',
            'key': 'ZpTLAZBy/zSupkcT0Lb18t+ouoLnve1cp4fqzeuEZzs='
        })

        picamera = MockPiCamera(images=['noqrcode.jpeg'])
        workflow = build(config=config, screen=Mock(), wifi=Mock(), res=self.res, led=Mock(), pi_camera=picamera)

        workflow.live.update_pulse()

        r = workflow.run().__next__()

        # verify photo and encryption
        self.assertEqual({}, r)

        workflow.stop()
