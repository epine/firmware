import os
import re
import threading
import time
import unittest
from http.server import HTTPServer
from unittest.mock import Mock

from epine.network import Client, Wifi
from tests.utils import MockConfig, MockServerHandler, get_resource


class TestClient(unittest.TestCase):
    def test_send_shot(self):
        key = '0xp3dPfmfHf2NkjJ0VFIhrk854fAAjfJ6UF9YwhOiCU='
        config = MockConfig(config={'key': key, 'server_url': 'http://localhost:6963'})
        client = Client(config)

        # Start server
        httpd = HTTPServer(('localhost', 6963), MockServerHandler)
        server = threading.Thread(None, httpd.handle_request)
        server.start()

        # Test
        img = get_resource('noqrcode.jpeg', bytes=True)
        res = client.send_shot(img=img)

        uuid4hex = re.compile('[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}', re.I)
        self.assertTrue(bool(uuid4hex.match(res['id'])))
        self.assertEqual(1600000000, res['date'])

    def test_send_ack(self):
        key = '0xp3dPfmfHf2NkjJ0VFIhrk854fAAjfJ6UF9YwhOiCU='
        config = MockConfig(config={'key': key, 'server_url': 'http://localhost:6967'})
        client = Client(config)

        # Start server
        httpd = HTTPServer(('localhost', 6967), MockServerHandler)
        server = threading.Thread(None, httpd.handle_request)
        server.start()

        # Test
        mes = "Hello, World"
        res = client.send_ack(message=mes)

        self.assertEqual('7d4045734a2fcebaaa4be624e411c51967b17547a269200ea547ab106f2adb9d', res["camera"])
        self.assertEqual('03675ac53ff9cd1535ccc7dfcdfa2c458c5218371f418dc136f2d19ac1fbe8a5', res["message"])
        self.assertEqual(1600000000, res['date'])

    def test_send_notify(self):
        key = '0xp3dPfmfHf2NkjJ0VFIhrk854fAAjfJ6UF9YwhOiCU='
        config = MockConfig(config={'key': key, 'server_url': 'http://localhost:6969'})
        client = Client(config)

        # Start server
        httpd = HTTPServer(('localhost', 6969), MockServerHandler)
        server = threading.Thread(None, httpd.handle_request)
        server.start()

        # Test
        img = get_resource('dog.jpg', bytes=True)
        res = client.send_notification(img=img, tags=['dog', 'animals'])

        uuid4hex = re.compile('[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}', re.I)
        self.assertTrue(bool(uuid4hex.match(res['id'])))
        self.assertEqual(1600000000, res["shot"]['date'])

    def test_listen_event(self):
        callback = Mock()

        key = "ZpTLAZBy/zSupkcT0Lb18t+ouoLnve1cp4fqzeuEZzs="
        config = MockConfig(config={'key': key, 'server_url': 'http://localhost:6969'})

        # Start server
        httpd = HTTPServer(('localhost', 6969), MockServerHandler)
        server = threading.Thread(None, httpd.handle_request)
        server.start()

        client = Client(config)
        client.add_event_listener(callback)
        client.start()

        time.sleep(5)
        client.stop()

        callback.assert_called_with("SETTINGS", {'id': '402b81b2f929c55c6844f96d3c98eb2e6f71553f2f7679dc85ef61a734081ae6', 'delayTimelapseMn': 10, 'delayDetectionMn': 15, 'disableObjects': [], 'notification': False})




class TestWifi(unittest.TestCase):
    def tearDown(self) -> None:
        if os.path.exists('wpa.config'):
            os.remove('wpa.config')

        if os.path.exists('cmd'):
            os.remove('cmd')

    def test(self):
        config = MockConfig(config=dict(wifi_ssid='my-ssid', wifi_pwd='my-pwd', wifi_country='MC'))
        display = Mock()
        wifi = Wifi(config=config, display=display, f='wpa.config', cmd='touch cmd')

        exp = """country=MC
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
ssid="my-ssid"
psk="my-pwd"
}"""

        # Test
        wifi.connect()
        with open('wpa.config', 'r') as f:
            content = f.read()
        self.assertEqual(exp, content)
        self.assertTrue(os.path.exists('cmd'))

        display.error.assert_called_once_with("Can't connect to my-ssid", id="wifi")
