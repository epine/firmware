import base64
import json
import unittest

from epine import crypto
from epine.crypto import decrypt
from tests.utils import generate_key


class TestCrypto(unittest.TestCase):
    def test_encrypt(self):
        clear = 'Hello world'
        key = generate_key()
        print(key)
        secret = crypto.encrypt(key, clear.encode())

        res = decrypt(key, secret).decode('ascii')
        self.assertEqual(clear, res)

    def test_encrypt(self):
        key = "ZpTLAZBy/zSupkcT0Lb18t+ouoLnve1cp4fqzeuEZzs="
        a = {'id': '402b81b2f929c55c6844f96d3c98eb2e6f71553f2f7679dc85ef61a734081ae6', 'delayTimelapseMn': 10,
         'delayDetectionMn': 15, 'disableObjects': [], 'notification': False}
        data = crypto.encrypt(key, json.dumps(a).encode())
        print(base64.b64encode(data).decode())

    def test_decrypt(self):
        tags = base64.urlsafe_b64decode("fFT0hQdFp5coSsUM4CgObHamlW/K55MTNbDN86Gx76p4gxmEI2TN")
        print([int(i) for i in tags])

        key = "ZxKpvkNiqIutE1T0VJpcQdo+i8H0WVN0BxXPXRkucww="
        print(decrypt(key, tags))