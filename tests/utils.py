import base64
import hashlib
import json
import os
import uuid
from io import BytesIO

import numpy as np
from http.server import BaseHTTPRequestHandler
from time import time, sleep
from typing import Tuple, Any, Dict, List, Generator, Union

from Crypto import Random
from Crypto.Cipher import ChaCha20_Poly1305
from PIL import Image

from epine.config import Config
from epine.display import Screen, Message


class MockConfig(Config):
    def __init__(self, config: Dict[str, Any]):
        super(MockConfig, self).__init__(config)

    def _save(self):
        pass


def generate_key() -> str:
    seed = Random.new().read(123)
    return base64.b64encode(hashlib.sha256(seed).digest()).decode()


def get_resource(name: str, bytes: bool = False) -> Union[Image.Image, bytes]:
    path = 'tests/' if 'tests' in os.listdir() else ''
    with open('%sres/%s' % (path, name), 'rb') as f:
        img = f.read()
    return img if bytes else Image.open(BytesIO(img))


class MockServerHandler(BaseHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.resume = True

    def do_GET(self):
        if "/event" in self.path:
            self._listen_event()

    def do_POST(self):
        print(f'SERVER RECEIVE {self.path}')
        if "/ack/" in self.path:
            self._send_ack()
        elif "/notification" in self.path:
            self._send_notify()
        elif "/shot" in self.path:
            self._send_shot()
        elif "/live" in self.path:
            self._send_shot()

    def _listen_event(self):
        sleep(4)
        event = {
            "type": "SETTINGS",
            "body":"5rduf1YQIobGHJuhedzN5oUN0G6nqgokXUZPsJ2Da5NcEpyursrgPzG8OjG/emffUziZEswS5ytffePS+Hx6nyzO4Ia45rQqrGcGq2j0wC8H5C4o/ViGJ5ESE9zAt6lf8W4pfd/uzUlf0nfbS5F/j6dHxl7q9vcfG884ggWAwj7VK3GY0jWgSByjXEXY00oYKZ5sWDSOyaJ2SHeG9PyQZoXe8VnpUM9X+Vd/t1y7caDDxDINXHJOkWDZBDAF8ZfDDu5i",
            "camera": "402b81b2f929c55c6844f96d3c98eb2e6f71553f2f7679dc85ef61a734081ae6"
        }

        res = json.dumps(event).encode()
        self.send_response(200)
        self.send_header('Content-Length', f'{len(res)}')
        self.end_headers()
        self.wfile.write(res)

    def _send_notify(self):
        _, _, _, _, key, _, tags = self.path.split('/')

        res = {"id": str(uuid.uuid4()), "tags": tags, "shot": {"date": 1600000000}}
        res = json.dumps(res).encode()
        self.send_response(200)
        self.send_header('Content-Length', f'{len(res)}')
        self.end_headers()
        self.wfile.write(res)

    def _send_ack(self):
        _, _, _, _, key, _, mes = self.path.split('/')

        res = {'camera': key, "message": mes, "date": 1600000000}
        res = json.dumps(res).encode()
        self.send_response(200)
        self.send_header('Content-Length', f'{len(res)}')
        self.end_headers()
        self.wfile.write(res)

    def _send_shot(self):
        print('Receive post')
        content_length = int(self.headers['Content-Length'])
        _, _, _, _, key, _ = self.path.split('/')
        data = self.rfile.read(content_length)

        self.send_response(200)
        res = {'id': str(uuid.uuid4()), 'date': 1600000000}
        res = json.dumps(res).encode()
        self.send_header('Content-Length', str(len(res)))
        self.end_headers()
        self.wfile.write(res)


class MockPiCamera:
    def __init__(self, images: List[str]):
        self.resolution = (0, 0)
        self.rotation = 0
        self.images = images

    def start_preview(self):
        pass

    def capture_continuous(self, stream, format, use_video_port) -> Generator[bytes, Any, None]:
        for name in self.images:
            img = get_resource(name, bytes=True)
            yield stream.write(img)

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class MockOut:
    def __init__(self, hash: str = None, printed: bool = False):
        self._image: Image = None
        self.hash = hash
        self.printed = printed

    def fill(self, n: int):
        pass

    @staticmethod
    def print(image: Image):
        for lines in np.asarray(image.rotate(180)):
            for char in lines:
                print("█" if char else " ", end='')
            print("")

    def show(self):
        if self._image:
            if self.printed:
                self.print(self._image)

            a = np.asarray(self._image).tobytes()
            hash = hashlib.sha1(a).hexdigest()
            if self.hash and hash != self.hash:
                self._image.show()
                raise ValueError(f"wrong hash exp={self.hash} != {hash}")

    def image(self, image: Image):
        self._image = image


class MockScreen(Screen):
    def __init__(self, delay: int = .1):
        self.splash = 0
        self.messages: List[Message] = list()
        self.delay = delay
        self.out = MockOut()

    def show_splash(self):
        self.splash += 1
        sleep(self.delay)

    def show_message(self, mes):
        self.messages.append(mes)
        sleep(self.delay)
