from adafruit_ssd1306 import SSD1306_I2C as OLED
from PIL import Image
from pdf417 import encode, render_image
import RPi.GPIO as gpio
import board
import busio
import base64

oled = OLED(128, 32, busio.I2C(board.SCL, board.SDA))

key = base64.b64decode('ZpTLAZBy/zSupkcT0Lb18t+ouoLnve1cp4fqzeuEZzs=')
print(key)
codes = encode(key, columns=1, numeric_compaction=True)
image = render_image(codes, padding=5).convert('1').resize((128, 32))

oled.image(image)
oled.show()