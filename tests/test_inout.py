import unittest
from time import sleep

from epine.inout import Camera
from tests.utils import MockPiCamera


class TestCamera(unittest.TestCase):

    def test(self):
        camera = Camera(MockPiCamera(images=['noqrcode.jpeg']))
        self.assertIsNotNone(camera.capture())
