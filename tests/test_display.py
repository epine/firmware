import os
import unittest
from pathlib import Path
from time import sleep
from unittest.mock import Mock, call

from epine.display import Screen, Display, Message
from tests.utils import MockOut, MockScreen


class TestScreen(unittest.TestCase):
    def setUp(self) -> None:
        self.font = Path(os.path.dirname(__file__)).parent / "epine/res/Roboto.ttf"

    def test_show_splash(self):
        screen = Screen(out=MockOut(hash="cb524335a06b13abba43f32e7596ad99286bbd5e"), font_path=self.font)
        screen.show_splash()

    def test_show_message(self):
        params = [
            ("error", "e41f5cd16c1e6c85db449be285b9b2b65bca3660"),
            ("valid", "e49f2dcd0e94d141a8074dce4deea10fb7a97c5d"),
            ("pending", "ed0c132dde00ec86b57f4113c03b3fb1dd7c8e83"),
            ("info", "1b574886054f04a93a972d50841c20d0142eca1b"),
            ('wifi', "6db1c4449f2ab6f33c18ae0f346c5a075bddc64b"),
            ("key", "5aa1b9d3db4ca0d1ac72ae713bfc957ef2c57f9c"),
            ("server", "5c7fd3c15ac4fe8266bfe805f79b2ebf31954cf8")
        ]
        for mode, hash in params:
            screen = Screen(out=MockOut(hash=hash), font_path=self.font)
            screen.show_message(Message(text="Bonjour le Monde", icon=mode))


class TestDisplay(unittest.TestCase):
    def test(self):
        screen = MockScreen()

        display = Display(screen=screen, config=Mock())
        sleep(0.05)
        self.assertEqual(1, screen.splash)

        error = Message(icon="error", text="Huge error")
        display.error(error.text, id="e")

        pending = Message(icon="pending", text="Process...")
        display.pending(pending.text, id="p")

        sleep(.45)
        valid = Message(icon="valid", text="All check")
        display.valid(valid.text, id='e')
        display.valid(valid.text, id='p')

        sleep(.4)
        self.assertEqual([error, pending, error, pending, valid, valid], screen.messages)

        display.stop()




